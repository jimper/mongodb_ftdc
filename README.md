# mongodb_ftdc

An utility to parse MongoDB FTDC files and to produce CSV files from them.  Processing time is very fast, and you can output only the metrics of interest.  The output format is CSV so then can be easily exchanged and shared with other applications.


**BETA!**
- Optimized to use a thread per chunk while parsing and at the time of writing multiple files.
- Able to parse and output metrics from FTDC files and directory.

**Roadmap**
- Create Python bindings.
- Display metrics - (Qt?)


**Usage:**
The utility uses a configuration file (config.yaml) to define the aliases for the metrics to use, as these are usually very large names.

```
  --version                             Print version string.
  --help                                produce help message.
  --verbose                             Display every ChunkVector initial data.
  --files arg                           FTDC file(s) to parse.
  --out-path arg (=./output)            Prefix/path to write output files.
  --config arg (=/home/jorge/.ftdc_parser)
                                        Configuration file.
  --metric-names                        Dump key names from metrics in file 
                                        (false)
  --separate                            Write metrics in separate files.(false)
  --start arg                           Start date and time (e.g.: 2020/12/31 
                                        13:15:21)
  --end arg                             End date and time (e.g.: 2020/12/31 
                                        13:15:21)
  --metrics arg                         List of metrics to dump to file, 
                                        separated by comma.
  --prefix arg                          Dump to file metrics whose prefix 
                                        matches.
  --dump-text                           Verbose textual metrics output.
  --timestamps                          Output date to files as timestamps.
  --metadata                            Output metadata only.

```


The common way to specify metrics to dump to CSV is to use an alias for a metric, defined in the config.yaml file.
```
./ftdcparser --files ../data/metrics.2020-06-21T09-57-16Z-00000  
--metrics ops_insert,ops_delete,ops_update,total_conn \
--config /home/jorge/CLionProjects/mongodb_ftdc/config.yaml 
```


Another way to specify metrics is to use a prefix:
```
./ftdcparser --files ../data/metrics.2020-06-21T09-57-16Z-00000  \
   --prefix serverStatus.opcounters \
   --config /home/jorge/CLionProjects/mongodb_ftdc/config.yaml 
```


The default output is a single CSV file with all the metrics
```
timestamp,t,systemMetrics.cpu.num_cpus,systemMetrics.cpu.user_ms,systemMetrics.cpu.nice_ms,systemMetrics.cpu.system_ms,systemMetrics.cpu.idle_ms,systemMetrics.cpu.iowait_ms,systemMetrics.cpu.irq_ms,systemMetrics.cpu.softirq_ms,systemMetrics.cpu.steal_ms,systemMetrics.cpu.guest_ms,systemMetrics.cpu.guest_nice_ms,systemMetrics.cpu.ctxt,systemMetrics.cpu.btime,systemMetrics.cpu.processes,systemMetrics.cpu.procs_running,systemMetrics.cpu.procs_blocked
2020/07/17 18:08:35,1595009315,4,2699219520,319030,647780880,245074391690,133179890,0,17760480,0,0,0,31904230533,1532806751,91430428,1,0
2020/07/17 18:08:36,1595009316,4,2699219520,319030,647780880,245074391690,133179890,0,17760480,0,0,0,31904230533,1532806751,91430428,1,0
2020/07/17 18:08:37,1595009317,4,2699219520,319030,647780880,245074391690,133179890,0,17760480,0,0,0,31904230533,1532806751,91430428,1,0
...

```

but by using `--separate` each metric will be written to a different file.

 
The `--verbose` option will output  every chunk metrics and values to the log. The deltas are not output.

```
[2020-12-03 10:35:23.204308] [0x00007f6edadf5740] [debug]   _id: 1593597498000
[2020-12-03 10:35:23.204312] [0x00007f6edadf5740] [debug]   type: 1
[2020-12-03 10:35:23.205636] [0x00007f6edadf5740] [debug]   start: 1593597498000
[2020-12-03 10:35:23.205642] [0x00007f6edadf5740] [debug]   serverStatus.start: 1593597498000
[2020-12-03 10:35:23.205646] [0x00007f6edadf5740] [debug]   serverStatus.pid: 2338
[2020-12-03 10:35:23.205652] [0x00007f6edadf5740] [debug]   serverStatus.uptime: 6.07955e+07
[2020-12-03 10:35:23.205656] [0x00007f6edadf5740] [debug]   serverStatus.uptimeMillis: 60795457530
[2020-12-03 10:35:23.205659] [0x00007f6edadf5740] [debug]   serverStatus.uptimeEstimate: 60795457
[2020-12-03 10:35:23.205663] [0x00007f6edadf5740] [debug]   serverStatus.localTime: 1593597498000
[2020-12-03 10:35:23.205667] [0x00007f6edadf5740] [debug]   serverStatus.asserts.regular: 0
[2020-12-03 10:35:23.205671] [0x00007f6edadf5740] [debug]   serverStatus.asserts.warning: 0
[2020-12-03 10:35:23.205676] [0x00007f6edadf5740] [debug]   serverStatus.asserts.msg: 0
[2020-12-03 10:35:23.205679] [0x00007f6edadf5740] [debug]   serverStatus.asserts.user: 433
[2020-12-03 10:35:23.205683] [0x00007f6edadf5740] [debug]   serverStatus.asserts.rollovers: 0
[2020-12-03 10:35:23.205687] [0x00007f6edadf5740] [debug]   serverStatus.connections.current: 18
[2020-12-03 10:35:23.205691] [0x00007f6edadf5740] [debug]   serverStatus.connections.available: 801
[2020-12-03 10:35:23.205695] [0x00007f6edadf5740] [debug]   serverStatus.connections.totalCreated: 1029952
[2020-12-03 10:35:23.205699] [0x00007f6edadf5740] [debug]   serverStatus.encryptionAtRest.encryptionEnabled: true
[2020-12-03 10:35:23.205704] [0x00007f6edadf5740] [debug]   serverStatus.extra_info.page_faults: 33304
[2020-12-03 10:35:23.205710] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.totalTime: 60795457528000
[2020-12-03 10:35:23.205714] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.currentQueue.total: 0
[2020-12-03 10:35:23.205727] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.currentQueue.readers: 0
[2020-12-03 10:35:23.205744] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.currentQueue.writers: 0
[2020-12-03 10:35:23.205749] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.activeClients.total: 56
[2020-12-03 10:35:23.205753] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.activeClients.readers: 0
[2020-12-03 10:35:23.205757] [0x00007f6edadf5740] [debug]   serverStatus.globalLock.activeClients.writers: 0
[2020-12-03 10:35:23.205763] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireCount.r: 19326006408
[2020-12-03 10:35:23.205768] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireCount.w: 2138922829
[2020-12-03 10:35:23.205772] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireCount.W: 53652092
[2020-12-03 10:35:23.205778] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireWaitCount.r: 748527
[2020-12-03 10:35:23.205785] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireWaitCount.w: 5371
[2020-12-03 10:35:23.205790] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.acquireWaitCount.W: 445216
[2020-12-03 10:35:23.205796] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.timeAcquiringMicros.r: 1777626045
[2020-12-03 10:35:23.205802] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.timeAcquiringMicros.w: 19178778
[2020-12-03 10:35:23.205807] [0x00007f6edadf5740] [debug]   serverStatus.locks.Global.timeAcquiringMicros.W: 741168429
[2020-12-03 10:35:23.205817] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireCount.r: 9516232545
[2020-12-03 10:35:23.205822] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireCount.w: 2135085611
[2020-12-03 10:35:23.205828] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireCount.R: 7845
[2020-12-03 10:35:23.205833] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireCount.W: 318577
[2020-12-03 10:35:23.205839] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireWaitCount.r: 543
[2020-12-03 10:35:23.205847] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireWaitCount.w: 992
[2020-12-03 10:35:23.205852] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireWaitCount.R: 1
[2020-12-03 10:35:23.205861] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.acquireWaitCount.W: 16192
[2020-12-03 10:35:23.205866] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.timeAcquiringMicros.r: 6972077
[2020-12-03 10:35:23.205872] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.timeAcquiringMicros.w: 781406
[2020-12-03 10:35:23.205878] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.timeAcquiringMicros.R: 218
[2020-12-03 10:35:23.205883] [0x00007f6edadf5740] [debug]   serverStatus.locks.Database.timeAcquiringMicros.W: 288711794
[2020-12-03 10:35:23.205889] [0x00007f6edadf5740] [debug]   serverStatus.locks.Collection.acquireCount.r: 9159652447
[2020-12-03 10:35:23.205894] [0x00007f6edadf5740] [debug]   serverStatus.locks.Collection.acquireCount.w: 1886922330
[2020-12-03 10:35:23.205901] [0x00007f6edadf5740] [debug]   serverStatus.locks.oplog.acquireCount.r: 305525074
[2020-12-03 10:35:23.205905] [0x00007f6edadf5740] [debug]   serverStatus.locks.oplog.acquireCount.w: 248323615
[2020-12-03 10:35:23.205912] [0x00007f6edadf5740] [debug]   serverStatus.logicalSessionRecordCache.activeSessionsCount: 0
...
```
