#!/bin/bash

MONGO_SRC_PATH=/home/jorge/mongo/mongo

DIRS=(
 ${MONGO_SRC_PATH}/build/opt/mongo/db/ftdc/
 ${MONGO_SRC_PATH}/build/opt/mongo/bson/util/
 ${MONGO_SRC_PATH}/build/opt/mongo/db/
 ${MONGO_SRC_PATH}/build/opt/third_party/s2/
 ${MONGO_SRC_PATH}/build/opt/third_party/
 ${MONGO_SRC_PATH}/build/opt/third_party/fmt/
 ${MONGO_SRC_PATH}/build/opt/third_party/boost-1.70.0/
 ${MONGO_SRC_PATH}/build/opt/third_party/zlib-1.2.11/
 ${MONGO_SRC_PATH}/build/opt/third_party/abseil-cpp-master/
 ${MONGO_SRC_PATH}/build/opt/third_party/IntelRDFPMathLib20U1/
 ${MONGO_SRC_PATH}/build/opt/third_party/unwind/
 ${MONGO_SRC_PATH}/build/opt/third_party/s2/util/coding/
 ${MONGO_SRC_PATH}/build/opt/third_party/murmurhash3/
 ${MONGO_SRC_PATH}/build/opt/third_party/pcre-8.42/
)

DEST_LIB_PATH=/home/jorge/CLionProjects/mongo_ftdc/lib

for DIR in ${DIRS[@]} ; do
   echo $DIR
   cd $DIR
   for lib in `ls *.a`; do
      ar -t $lib | xargs ar rvs $lib-new.a && mv $lib-new.a  ${DEST_LIB_PATH}/$lib ;
   done
done


