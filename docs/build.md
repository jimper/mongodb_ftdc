
You will need CMake and Boost to build this

``` 
sudo apt-get install cmake libboost-dev libboost-log-dev libboost-log-program-options
```


Plus libbson

```
sudo apt-get install libbson-1.0-0
```

or in OSX

```
brew install mongo-c-driver
```

Also, yaml-cpp

```
sudo apt install libyaml-cpp-dev
```

or built locally downloading and building
```
https://github.com/mongodb/mongo-c-driver/tree/master/src/libbson
https://github.com/jbeder/yaml-cpp/
```


###Manually building Boost

Get the version of Boost that you require. This is for 1.55 but feel free to change or manually download yourself (Boost download page):
```bash
wget -O boost_1_76_0.tar.gz https://sourceforge.net/projects/boost/files/boost/1.76.0/boost_1_76_0.tar.gz/download
tar xzvf boost_1_76_0.tar.gz
cd boost_1_76_0/
```
Get the required libraries

```bash
sudo apt-get update
sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev libbz2-dev
```
Boost's bootstrap setup:

```shell
./bootstrap.sh --prefix=/usr/
```


Install boost in:

```shell
sudo ./b2 --with=all  install
sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf.d/local.conf'


sudo ldconfig
 ```
