//
// Created by jorge on 12/16/20.
//

#include "include/Dataset.h"
#include <boost/log/core.hpp>

bool
Dataset::has(const std::string & metricName) {
    if (ChunkVector.size() > 0) {
       return ChunkVector[0]->hasMetric(metricName);
    }
    return false;
}

size_t
Dataset::getFullMetric(const std::string &metricName, uint64_t  **data) {

    if (ChunkVector.size() > 0) {
        *data = new uint64_t [samplesInDataset];

        // fill them now
        uint64_t *p = *data;

        for (auto &m: ChunkVector) {
            m->getMetric(metricName, p);
            p += m->getSamplesCount();
        }
        return samplesInDataset;
    }
    return 0;
}

size_t
Dataset::getMetricNames(std::vector<std::string> & metricNames) {
    if (ChunkVector.size() > 0) {

        ChunkVector[0]->getMetricNames(metricNames);
        return metricNames.size();
    }
    else {
        return 0;
    }
}

void
Dataset::addChunk(int64_t id, int32_t type, Chunk *pChunk) {
    this->addChunk(pChunk);
}

void
Dataset::addChunk(Chunk *pChunk) {

    // Critical section
    mu.lock();

    this->ChunkVector.emplace_back(pChunk);
    // total size of samplesInDataset
    samplesInDataset += pChunk->getSamplesCount();

    mu.unlock();
}



void Dataset::sortChunks() {

    struct {
        bool operator()(Chunk *a, Chunk *b) const { return a->getId() < b->getId(); }
    } compare;

    std::sort(ChunkVector.begin(), ChunkVector.end(), compare);

}
