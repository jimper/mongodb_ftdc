//
// Created by jorge on 12/16/20.
//

#ifndef FTDCPARSER_DATASET_H
#define FTDCPARSER_DATASET_H

#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>
#include "Chunk.h"

class Dataset {

public:
    Dataset() : samplesInDataset(0) {}
    void addChunk(Chunk *pChunk);
    void addChunk(int64_t id, int32_t type, Chunk *pChunk);
    size_t getChunkCount() { return ChunkVector.size(); }
    Chunk *getChunk(size_t n) {  return (n<ChunkVector.size()) ? ChunkVector[n] : nullptr; }
    bool has(const std::string &basicString);
    size_t getFullMetric(const std::string &name,  uint64_t  **data);
    size_t getMetricNames(std::vector< std::string> & metricNames);
    size_t SortChunksByTimestamp(void);

    std::vector<Chunk *> getChunkVector() { return ChunkVector;}

    void sortChunks();

private:
    std::vector<Chunk*> ChunkVector;
    size_t samplesInDataset;
    boost::mutex mu;

};


#endif //FTDCPARSER_DATASET_H
