//
// Created by jorge on 11/2/20.
//

#ifndef FTDCPARSER_FTDCPARSER_H
#define FTDCPARSER_FTDCPARSER_H

#include "iostream"
#include "vector"
#include "Chunk.h"
#include "Dataset.h"
#include "ParserTasksList.h"
#include <string_view>
#include <string>

// From libbson
#include <bson.h>

#include <boost/program_options.hpp>

class FTDCParser {
public:
    bson_reader_t* open(std::string file_path);
    int parseFiles(std::vector<std::string> &file_paths, bool chunk_txt, bool metadata, bool metric_names);
    int parseFiles(std::string file_paths, bool chunk_txt, bool metadata, bool metric_names);

    int parseInfoChunk (const bson_t *bson);
    std::vector<std::string> getMetricsNamesPrefixed(std::string prefix) ;
    std::vector<std::string> getMetricsNames();
    size_t getMetrics(const std::string& name,  uint64_t **metrics);

    std::vector<Chunk*> getChunks() { return dataSet.getChunkVector(); }

    std::string getMetadata() { return metadata; }

private:
     ParserTasksList parserTasks;
     Dataset dataSet;
     std::string metadata;
};




#endif //FTDCPARSER_FTDCPARSER_H