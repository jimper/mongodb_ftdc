### **PyFTDC**

###### Python3 bindings for the FTDC parser library.


These are bindings for the FTDC Parser library.  Implemented using Boost::Python and Boost::Numpy

#### Build
cmake --build /m_ftdc/cmake-build-release --target pyftdc 

They provide basic calls for a parser:

```C++
  


BOOST_PYTHON_MODULE(pyftdc)  // This is the same name needed to import on the python source.
{
    p::class_<ParserClass>("ParserClass", p::init<std::string>())
        .def("parse_file", &ParserClass::parseFile)
        .def("parse_dir", &ParserClass::parseDir)
        .def("get_timestamps", &ParserClass::getTimestamps)
        .def("get_metrics", &ParserClass::getMetric)
        .def("get_metrics_names", &ParserClass::getMetricNames)
    ;

}
```

and can be used as 

```python
import pyftdc

p = pyftdc.ParserClass("Pavel")
file_path = '/data/test/diagnostic.data'

parsed = p.parse_dir(file_path)

print(f"Parser = {parsed}")
metric_names = p.get_metrics_names()

print(metric_names)

a_metric = 'systemMetrics.netstat.Tcp:RetransSegs'
metric = p.get_metrics(a_metric)

print(f"There are {len(metric)} values in  {a_metric} metric")

```