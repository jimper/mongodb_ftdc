#include <FTDCParser.h>

#include <boost/filesystem/operations.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <string>

typedef std::vector<uint64_t> Metrics;
typedef std::vector<std::string> MetricNames;

namespace p = boost::python;
namespace logging = boost::log;

struct ParserClass
{
    FTDCParser *pParser;
    Metrics timeStamps;
    std::vector<std::string> names;

    explicit ParserClass() {
        pParser = new FTDCParser();

        Py_Initialize();

        // Only log errors and fatalities
        logging::core::get()->set_filter(logging::trivial::severity == logging::trivial::error
         || logging::trivial::severity == logging::trivial::fatal);
    };

    int parseFile(std::string file) {
        int n = pParser->parseFiles(file, false, false, false);
        return n;
    }

    int parseDir(std::string dir) {

        // if it exists an it is a directory, pop and push contents
        if (boost::filesystem::exists(dir) && boost::filesystem::is_directory(dir)) {

            std::vector<std::string> fileList;
            for (auto&& fileInPath : boost::filesystem::directory_iterator(dir))
                fileList.push_back(fileInPath.path().string());

            // Not really necessary.
            std::sort(fileList.begin(), fileList.end());
            int n = pParser->parseFiles(fileList, false, false, false);
            return n;
        }
        return -1;
    }

    Metrics getTimestamps() {
        if (timeStamps.empty()) {
            uint64_t *ts_metric;
            size_t  samples = pParser->getMetrics("start", &ts_metric);
            for (size_t i=0;i<samples; ++i) timeStamps[i] = ts_metric[i];
            //delete ts_metric;
        }
        return timeStamps;
    }

    Metrics getMetric(std::string metric) {
        uint64_t  *metric_values;
        size_t samples = pParser->getMetrics(metric, &metric_values);

        std::vector<uint64_t> return_metrics(samples);
        for (size_t i=0;i<samples; ++i) return_metrics[i] = metric_values[i];
        //delete [] metric_values;

        return return_metrics;
    }

    MetricNames getMetricNames() {
        names = pParser->getMetricsNames();
        return names;
    }

    std::string getMetadata() {
        return pParser->getMetadata();
    }
};


BOOST_PYTHON_MODULE(pyftdc)  // This is the same name needed to import on the python source.
{
    p::class_<ParserClass>("ParserClass")
        .def("parse_file", &ParserClass::parseFile)
        .def("parse_dir", &ParserClass::parseDir)
        .def("get_timestamps", &ParserClass::getTimestamps)
        .def("get_metrics", &ParserClass::getMetric)
        .def("get_metrics_names", &ParserClass::getMetricNames)
        .def("get_metadata", &ParserClass::getMetadata)
    ;

    p::class_<MetricNames>("MetricNames")
            .def(p::vector_indexing_suite<MetricNames>() );
    p::class_<Metrics>("Metrics")
            .def(p::vector_indexing_suite<Metrics>() );
}





